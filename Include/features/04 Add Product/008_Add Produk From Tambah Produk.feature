#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Add produk from tambah produk
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Title of your scenario outline
    Given User go to URL seconhand and do input <user_email> and <user_password> for login
    When User click humberger and tambah produk button
    And User SA do input <productName> and <productDescription> in tambah produk
    And User SA set <productPrice> in product price field in tambah produk
    And User SA select <productCategory> for product category in tambah produk
    And User SA upload <productImage> in tambah produk
    And User click the Terbitkan Button in tambah produk
    Then User verifying productDetail in tambah produk

    Examples: 
      | productName  	| productPrice 	| productDescription  |	productCategory		|	productImage	|	user_email	| user_password	|
      | Produk Shan		|	1000					| second 							|	2									|	pict.png			|	email_user	|	pass_user	|