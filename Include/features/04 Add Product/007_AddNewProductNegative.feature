#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@AddProduct
Feature: Add New Product Negative
  As a user I want to Add Product for Negative Case

  @TC007
  Scenario Outline: TC007 - User want to add product without input mandatory field
    Given User go to URL and do input <user_email> and <user_password> for login positive
    When User click the Add Product Button Positive
    Then User click the Terbitkan Negative Button
    
    Examples: 
      | user_email  | user_password |
      | emailUser |     passwordUser | 
