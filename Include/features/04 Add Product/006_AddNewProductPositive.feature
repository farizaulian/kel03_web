#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Title of your scenario outline
    Given User go to URL and do input <user_em> and <user_pa> for login
    When User click the Add Product Button
    And User do input <productName> and <productDescription>
    And User set <productPrice> in product price field 
    And select <productCategory> for product category
    And User upload <productImage>
    And User click the Terbitkan Button
    Then User verifying productDetail

    Examples: 
      | productName  	| productPrice 	| productDescription  |	productCategory		|	productImage	|	user_em	| user_pa	|
      | Produk Widi		|	1000					| second 							|	2									|	pict.png			|	email_user	|	pass_user	|