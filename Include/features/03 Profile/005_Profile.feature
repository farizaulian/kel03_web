#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Update Profile in Secondhand
  I want to try update profile

  @tag1
  Scenario Outline: User can update their profile
    Given User go to URL and input <email> and <password> in the field
    When User click profile button
    And User click nickname button
    And User change the <username>
    And User click Simpan Button
    Then User Profile Updated
    

    Examples: 
      | username  			| email 			| password 	|
      | update_username | user_email 	| user_pass	|