#Author: your.email@your.domain.com
#Keywords Summary : Test the positive sign-up flow on the website
#Feature: Signing up on the website with valid information
#Scenario: User is able to sign up with a unique email and password and can access their profile details.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@SignUpPositive
Feature: Sign up with valid information

  Scenario Outline: User successfully signs up with valid username, email, and password
    Given I open a browser and navigate to the homepage
    When I click the login page link
    And I click the sign-up link
    Then I should be able to enter a username "<username>"
    And I generate and enter a unique email
    And I enter a valid password "<password>"
    And I click the submit button
    Then I should see the profile icon first
    And I click on the profile icon first
    And I click on the profile picture
    Then I should see the Lengkapi Info Akun element first

    Examples:
      | username        | password     |
      | Kelompok 03 Web | aeHFOx8jV/A= |