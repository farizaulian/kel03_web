#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Negative Signup for SecondHand Application

  Scenario: Attempt to Signup with Invalid Email Format
    Given I open the SecondHand application
    When I navigate to the login page
    And I choose to register for a new account
    And I enter the name as "kel03"
    And I enter an invalid email as "kel03gmail.com"
    And I enter a password
    And I submit the registration form
    Then I should see the registration failed message