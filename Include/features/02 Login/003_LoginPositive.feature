#Author: your.email@your.domain.com
#Keywords Summary : Test the positive login flow on the website
#Feature: Logging into the website with valid credentials
#Scenario: User is able to log in with correct email and password and access profile details.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@LoginPositive
Feature: Login with valid credentials

  Scenario Outline: User successfully logs in with valid email and password
    Given I open a browser and navigate to the login page
    When I enter a valid email "<email>" and a valid password "<password>"
    Then I should be able to click the login button
    And I should see the profile icon
    And I click on the profile icon
    And I click on the profile information
    Then I should see the Lengkapi Info Akun element
    
    Examples:
      | email            | password     |
      | kel03_web@mail.com| O9Z43x2lAxM= |