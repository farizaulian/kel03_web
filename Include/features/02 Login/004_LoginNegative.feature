#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Negative Case Login in Secondhand
  I want to try negative case login in secondhand

  @tag1
  Scenario Outline: User can't login to secondhand
    Given User go to the Secondhand URL
    When User input <email> and <password> in the field
    And User click the login button
    Then Invalid credential pop up will appear

    Examples: 
      | email  			| password 		|
      | user_email	| user_pass		|