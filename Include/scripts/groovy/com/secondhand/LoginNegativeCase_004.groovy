package com.secondhand
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LoginNegativeCase_004 {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User go to the Secondhand URL")
	def secondhandURL() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
	}

	@When("User input (.*) and (.*) in the field")
	def userInputEmailPass(String email, password) {
		WebUI.click(findTestObject('Object Repository/TC004_Login_Negative/TC004A_InputDoesNotExistUser/Page_SecondHand/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/TC004_Login_Negative/TC004A_InputDoesNotExistUser/Page_SecondHand/input_Email_useremail'),
				'walfreeday@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/TC004_Login_Negative/TC004A_InputDoesNotExistUser/Page_SecondHand/input_Password_userpassword'),
				'8SQVv/p9jVScEs4/2CZsLw==')
	}

	@And("User click the login button")
	def userClickLoginButton() {
		WebUI.click(findTestObject('Object Repository/TC004_Login_Negative/TC004A_InputDoesNotExistUser/Page_SecondHand/input_Password_commit'))
	}

	@Then("Invalid credential pop up will appear")
	def invalidCredentialPopup() {
		WebUI.click(findTestObject('TC004_Login_Negative/TC004A_InputDoesNotExistUser/Page_SecondHand/button_Invalid Email or password_btn-close'))
	}
}