package com.secondhand
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SignUpNegative_002 {
    
    @Given("I open the SecondHand application")
    def iOpenTheSecondHandApplication() {
        WebUI.openBrowser('')
        WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
    }

    @When("I navigate to the login page")
    def iNavigateToTheLoginPage() {
        WebUI.click(findTestObject('Object Repository/TC002_SignUp_Negative/Page_SecondHand/a_Masuk'))
    }

    @When("I choose to register for a new account")
    def iChooseToRegisterForANewAccount() {
        WebUI.click(findTestObject('Object Repository/TC002_SignUp_Negative/Page_SecondHand/a_Daftar di sini'))
    }

    @When("I enter the name as {string}")
    def iEnterTheName(String name) {
        WebUI.setText(findTestObject('Object Repository/TC002_SignUp_Negative/Page_SecondHand/input_Name_username'), name)
    }

    @When("I enter an invalid email as {string}")
    def iEnterAnInvalidEmail(String email) {
        WebUI.setText(findTestObject('Object Repository/TC002_SignUp_Negative/Page_SecondHand/input_Email_useremail'), email)
    }

    @When("I enter a password")
    def iEnterAPassword() {
        WebUI.setEncryptedText(findTestObject('Object Repository/TC002_SignUp_Negative/Page_SecondHand/input_Password_userpassword'), 'O9Z43x2lAxM=')
    }

    @When("I submit the registration form")
    def iSubmitTheRegistrationForm() {
        WebUI.click(findTestObject('Object Repository/TC002_SignUp_Negative/Page_SecondHand/input_Password_commit'))
        WebUI.delay(5)
    }

    @Then("I should see the registration failed message")
    def iShouldSeeTheRegistrationFailedMessage() {
        WebUI.verifyElementVisible(findTestObject('Object Repository/TC002_SignUp_Negative/Page_SecondHand/input_Password_commit'))
    }
}