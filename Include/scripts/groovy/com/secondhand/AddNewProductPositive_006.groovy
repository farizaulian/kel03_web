package com.secondhand
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AddNewProductPositive_006 {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User go to URL and do input (.*) and (.*) for login")
	def userLogin(String user_email, user_pass) {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

		WebUI.click(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/input_Email_useremail'), 'kel03_03seller@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/input_Password_userpassword'),
				'zlMKVWibZXHkmLiVPR2ufQ==')
		WebUI.click(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/input_Password_commit'))
	}

	@When("User click the Add Product Button")
	def addProductButton () {
		WebUI.click(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/i_Next_bi bi-plus fs-2 me-3'))
	}

	@And("User do input (.*) and (.*)")
	def inputNamePriceDescription(String productName, productDescription) {
		WebUI.setText(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/input_Nama Produk_productname'), 'produk wididay')
		WebUI.setText(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/textarea_Deskripsi_productdescription'),
				'jalan')
	}

	@And("User set (\\d+) in product price field")
	def productPrice(int productPrice) {
		WebUI.setText(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/input_Harga Produk_productprice'),
				'1000')
	}

	@And("select (\\d+) for product category")
	def productCategorySelection(int productCategory) {
		WebUI.selectOptionByIndex(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/select_Pilih KategoriHobiKendaraanBajuElektronikKesehatan'),
				'2', FailureHandling.CONTINUE_ON_FAILURE)
	}

	@And("User upload (.*)")
	def uploadProductImage (String productImage) {
		WebUI.uploadFile(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/i_Deskripsi_bi bi-plus display-6'), '/Users/telkom/Desktop/dummy.png')

		WebUI.delay(5)
	}

	@And("User click the Terbitkan Button")
	def clickTerbitkan () {
		WebUI.click(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/label_Terbitkan'))
	}

	@Then("User verifying productDetail")
	def verifyProductImage() {
		WebUI.verifyElementVisible(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/page preview upload product'))
	}
}