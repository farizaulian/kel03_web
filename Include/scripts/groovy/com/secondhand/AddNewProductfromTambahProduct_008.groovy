package com.secondhand
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AddNewProductfromTambahProduct_008 {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User go to URL seconhand and do input (.*) and (.*) for login")
	def userLoginProduct(String user_email, user_password) {
		WebUI.openBrowser('')
	
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		
		WebUI.maximizeWindow()
		
		WebUI.click(findTestObject('TC008 Add Product for Sale Positive/a_Masuk'))
		
		WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Email_useremail'), 'Shantika113@gmail.com')
		
		WebUI.setEncryptedText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Password_userpassword'), 
		    'aeHFOx8jV/A=')
		
		WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Password_commit'))
	}

	@When("User click humberger and tambah produk button")
	def TambahProdukButton() {
		WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/i_Produk Saya_bi bi-list-ul me-4 me-lg-0'))
	
		WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/i_Terjual_bi bi-plus display-3'))
	}

	@And("User SA do input (.*) and (.*) in tambah produk")
	def inputNamePriceDescription1(String productName, productDescription) {
		WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Nama Produk_productname'), 'HP Samsung')
		WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/textarea_Deskripsi_productdescription'),
			'Samsung A50S')
	}
	
	@And("User SA set (\\d+) in product price field in tambah produk")
	def productPrice1(int productPrice) {
		WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Harga Produk_productprice'), '2000000')
	}
	
	@And("User SA select (\\d+) for product category in tambah produk")
	def productCategorySelection1(int productCategory) {
		WebUI.selectOptionByValue(findTestObject('Object Repository/TC008 Add Product for Sale Positive/select_Pilih KategoriHobiKendaraanBajuElekt_20972a'), 
    '4', true)
	}
	
	@And("User SA upload (.*) in tambah produk")
	def uploadProductImage1(String productImage) {
		WebUI.click(findTestObject('TC008 Add Product for Sale Positive/add_gambar_stock'))
		
		WebUI.delay(15)
	}
	
	@And("User click the Terbitkan Button in tambah produk")
	def clickTerbitkanProduk () {
		WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/label_Preview'))
		
		WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Password_commit'))
	}
	
	@Then("User verifying productDetail in tambah produk")
	def verifyTambahProduk () {
		WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/i_Produk Saya_bi bi-list-ul me-4 me-lg-0'))
		
		WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/section_Daftar Jual Saya                   _0b6c3c'))
	}
}