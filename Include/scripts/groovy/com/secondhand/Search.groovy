package com.secondhand
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Search {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User open URL and input (.*) and (.*) in the field")
	def UsertoLogin (String Email, Password) {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Object Repository/TC013-Search_Negative/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/TC013-Search_Negative/input_Email_useremail'), 'kel03_03@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/TC013-Search_Negative/input_Password_userpassword'), 'O9Z43x2lAxM=')
		WebUI.click(findTestObject('Object Repository/TC013-Search_Negative/input_Password_commit'))
	}

	@When("User input (.*) in the field")
	def User_check_for_the_text_in_step (String text) {
		WebUI.setText(findTestObject('Object Repository/TC013-Search_Negative/input_Produk Saya_q'), '}{}{}{}{}{}{}{}{')
	}

	@Then("User click the Search button")
	def userClickLoginButton()  {
		WebUI.click(findTestObject('Object Repository/TC013-Search_Negative/i_Produk Saya_bi bi-search input-group-text_db316a'))

	}
}