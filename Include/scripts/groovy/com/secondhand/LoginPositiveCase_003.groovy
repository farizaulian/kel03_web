package com.secondhand
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LoginPositiveCase_003 {

	@Given("I open a browser and navigate to the login page")
	def open_browser_and_navigate() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
	}

	@When("I enter a valid email {string} and a valid password {string}")
	def enter_valid_email_and_password(String email, String password) {
		WebUI.click(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/input_Email_useremail'), email)
		WebUI.setEncryptedText(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/input_Password_userpassword'), password)
	}

	@Then("I should be able to click the login button")
	def click_login_button() {
		WebUI.click(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/input_Password_commit'))
	}

	@And("I should see the profile icon")
	def see_profile_icon() {
		// Verify the profile icon is visible
		WebUI.verifyElementPresent(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/i_Keluar_bi bi-person me-4 me-lg-0'), 5)
	}

	@And("I click on the profile icon")
	def click_profile_icon() {
		WebUI.click(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/i_Keluar_bi bi-person me-4 me-lg-0'))
	}

	@And("I click on the profile information")
	def click_profile_information() {
		WebUI.click(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/div_kel03_web'))
	}

	@Then("I should see the Lengkapi Info Akun element")
	def verify_element() {
		WebUI.verifyElementVisible(findTestObject('Object Repository/TC003_Login_Positive/Page_SecondHand/div_Lengkapi Info Akun'))
	}
}