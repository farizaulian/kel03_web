package com.secondhand
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import internal.GlobalVariable
import java.util.Date

class SignUpPositive_001 {
    
    @Given("I open a browser and navigate to the homepage")
    def open_browser_and_navigate() {
        WebUI.openBrowser('')
        WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
    }

    @When("I click the login page link")
    def click_login_page_link() {
        WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/a_Masuk'))
    }

    @And("I click the sign-up link")
    def click_sign_up_link() {
        WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/a_Daftar di sini'))
    }

    @Then("I should be able to enter a username {string}")
    def enter_username(String username) {
        WebUI.setText(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Name_username'), username)
    }

    @And("I generate and enter a unique email")
    def generate_and_enter_email() {
        Date email = new Date()
        String emailDoctor = email.format('yyyyMMddHHmmss')
        def email_code = ('kel03_web' + emailDoctor) + '@gmail.com'
        GlobalVariable.emailVar = email_code
        WebUI.setText(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Email_useremail'), GlobalVariable.emailVar)
    }

    @And("I enter a valid password {string}")
    def enter_password(String password) {
        WebUI.setEncryptedText(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Password_userpassword'), password)
    }

    @And("I click the submit button")
    def click_submit_button() {
        WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Password_commit'))
    }

    @Then("I should see the profile icon first")
    def see_profile_icon() {
        WebUI.verifyElementPresent(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/i_Keluar_bi bi-person me-4 me-lg-0'), 5)
    }

    @And("I click on the profile icon first")
    def click_profile_icon() {
        WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/i_Keluar_bi bi-person me-4 me-lg-0'))
    }

    @And("I click on the profile picture")
    def click_profile_picture() {
        WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/img_Profil Saya_img-avatar w-100 rounded-4 mb-2'))
    }

    @Then("I should see the Lengkapi Info Akun element first")
    def verify_element() {
        WebUI.verifyElementVisible(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/div_Lengkapi Info Akun'))
    }
}