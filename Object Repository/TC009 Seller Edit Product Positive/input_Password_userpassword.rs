<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_userpassword</name>
   <tag></tag>
   <elementGuidId>7b37fcbb-080e-4ad8-a2cd-c23a45bf5f49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='user_password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#user_password</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b024265d-1be1-416e-a221-aba54a49a306</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>0eeb20ab-ccbb-44aa-8229-cd7623306345</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>current-password</value>
      <webElementGuid>a803d17b-d801-4d02-8cba-0cb2ed0b7df6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>759c3a28-afca-4d85-86f6-94f4b536b057</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Masukkan password</value>
      <webElementGuid>3a49d907-b773-42b1-bd2b-54f8132c548f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>759d7673-b6f7-4fea-9af0-7e1f32e4f148</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user[password]</value>
      <webElementGuid>83365579-9a3a-4b10-9b59-94868ea34835</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_password</value>
      <webElementGuid>d3443367-fc7e-4a25-b32d-a6854e3e5bb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user_password&quot;)</value>
      <webElementGuid>72916f35-6050-415c-b04a-119e5ded559e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='user_password']</value>
      <webElementGuid>0b67d5e9-4613-415c-b992-dd9151553aaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[2]/input</value>
      <webElementGuid>c5c9b4f9-471d-4c0e-8bb2-e3e501bb2c9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>12007bb0-89ca-4e0c-bfeb-9d764b9a58f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Masukkan password' and @type = 'password' and @name = 'user[password]' and @id = 'user_password']</value>
      <webElementGuid>ac3d3302-0359-4acb-afa8-e00689785e74</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
