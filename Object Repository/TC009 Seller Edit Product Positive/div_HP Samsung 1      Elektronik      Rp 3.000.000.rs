<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_HP Samsung 1      Elektronik      Rp 3.000.000</name>
   <tag></tag>
   <elementGuidId>7c88cdea-5eee-4355-839a-b1de7df34e59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.000.000'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f85405a4-49fb-4c8e-9028-dcb6d8d14fc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-12 col-lg-4</value>
      <webElementGuid>c6c8be58-e7f0-4417-a155-aec9d08706f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
  
      

    
      HP Samsung 1
      Elektronik
      Rp 3.000.000
    
  

            </value>
      <webElementGuid>6edc9cd2-ed15-4acd-a14a-1f5a0320d1da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]</value>
      <webElementGuid>bed155d0-a01f-4105-ad2f-6f30a47e6a52</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.000.000'])[1]/following::div[1]</value>
      <webElementGuid>38cd49be-93c3-4fb2-b9cf-e420bc003e9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP Samsung'])[1]/following::div[1]</value>
      <webElementGuid>8efd9af1-d221-4ccb-8bda-f674aac326f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]</value>
      <webElementGuid>18cd31b0-18f7-4b82-8c57-01d9919c2dc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
              
  
      

    
      HP Samsung 1
      Elektronik
      Rp 3.000.000
    
  

            ' or . = '
              
  
      

    
      HP Samsung 1
      Elektronik
      Rp 3.000.000
    
  

            ')]</value>
      <webElementGuid>b8914188-ad27-4a41-8715-cda6f1cf91d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
