<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Produk Saya_navbar-brand px-5 py-3</name>
   <tag></tag>
   <elementGuidId>715d8180-b8df-4580-b5f6-5f5abe9f6717</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(@href, '/')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.navbar-brand.px-5.py-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f608e9e7-59bc-4ca0-bc13-353c6960d858</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-brand px-5 py-3</value>
      <webElementGuid>d528119e-e050-4b37-a307-8dd2b2eea55f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/</value>
      <webElementGuid>3b39a177-ce53-412e-891c-e947fdf44f35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]/a[@class=&quot;navbar-brand px-5 py-3&quot;]</value>
      <webElementGuid>503018bb-dbbb-45b5-bba0-6691fe740e2a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/')]</value>
      <webElementGuid>8ed3bd3c-9768-4c9d-b3bf-571d31ad3d55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>c6c310c3-5cc4-4ce4-badb-c2a22272a837</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/']</value>
      <webElementGuid>d243a574-e1e9-4638-b9cf-3275dfccc3d9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
