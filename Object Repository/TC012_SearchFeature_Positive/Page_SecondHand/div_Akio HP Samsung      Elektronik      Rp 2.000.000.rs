<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Akio HP Samsung      Elektronik      Rp 2.000.000</name>
   <tag></tag>
   <elementGuidId>59e851d6-d799-4f94-adc8-beb56af49a60</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3ea98bb4-f270-4c5e-92ba-676f2b4eea53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-12 col-md-4 col-lg-3</value>
      <webElementGuid>8333c9c2-4f0c-4219-bedd-e10334daae71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
  
      

    
      Akio HP Samsung
      Elektronik
      Rp 2.000.000
    
  

      </value>
      <webElementGuid>702f2204-e68a-47c1-8454-085d1e647e05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]</value>
      <webElementGuid>f35159a8-88fa-4d6a-89b4-da3b54ecbfde</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div[6]</value>
      <webElementGuid>88b3da23-e444-42ae-a4e5-49fa2dd2fb59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.000.000'])[4]/following::div[1]</value>
      <webElementGuid>303aaf8d-a3ea-4067-ab16-69938dc66b6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP Samsung'])[4]/following::div[1]</value>
      <webElementGuid>80f05bc2-3781-4c14-980e-99d860c9110d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]</value>
      <webElementGuid>2fc45bba-fd26-4dc8-a5db-c1fb34fc7942</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
  
      

    
      Akio HP Samsung
      Elektronik
      Rp 2.000.000
    
  

      ' or . = '
        
  
      

    
      Akio HP Samsung
      Elektronik
      Rp 2.000.000
    
  

      ')]</value>
      <webElementGuid>20e1cb9f-36d6-4e52-8a41-3178a1ffa82c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
