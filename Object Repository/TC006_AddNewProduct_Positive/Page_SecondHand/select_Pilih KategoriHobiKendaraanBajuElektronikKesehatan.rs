<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Pilih KategoriHobiKendaraanBajuElektronikKesehatan</name>
   <tag></tag>
   <elementGuidId>3f9e4774-74f6-4c0e-979e-e92e80e7ebe8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#product_category_id</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_category_id']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='product_category_id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>062c7fb0-1d4d-4b9c-b06e-9b0818cf0810</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-select rounded-4 p-3</value>
      <webElementGuid>a60e3cd3-d1ef-4194-a347-e837c1eed42d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>bc1a0353-650d-422d-b8cc-73cebdb1fa97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>product[category_id]</value>
      <webElementGuid>d04e5114-bdbd-47d5-afc5-dff4ac0129a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_category_id</value>
      <webElementGuid>969ebdea-6e82-4475-a313-e769c96eae15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan</value>
      <webElementGuid>c4562c53-f19f-4420-97bc-4f7800120fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product_category_id&quot;)</value>
      <webElementGuid>4989de8a-0964-4ca1-8063-df9898a23f0e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='product_category_id']</value>
      <webElementGuid>0fbbafcc-328c-4560-96d9-cc9bb10db864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::select[1]</value>
      <webElementGuid>f0e70115-fcf5-40f3-a3e1-efe7b6a21708</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Produk'])[1]/following::select[1]</value>
      <webElementGuid>b4eb307f-0458-4d6f-8987-b26a96059d3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/preceding::select[1]</value>
      <webElementGuid>62ad6246-5ad7-4e82-b414-08106deba84d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/preceding::select[1]</value>
      <webElementGuid>e12c3605-0738-48a0-96bf-142330712247</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>22f6142e-2df9-4414-94f9-c0d975bbdd8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'product[category_id]' and @id = 'product_category_id' and (text() = 'Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan' or . = 'Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan')]</value>
      <webElementGuid>3d453e3d-8526-4e2d-93d4-9d07b7ead4d7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
