<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Telusuri Kategori</name>
   <tag></tag>
   <elementGuidId>41535d5c-c2a8-4f90-8dfe-fe7fd47199fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/following::section[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>section.container</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>8f3e1224-2bc2-4dc7-8714-f22f0e8f39c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>59f991e6-18cb-4a5b-a66c-110e32add153</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  Telusuri Kategori

  
    
      
      Semua

      
        
        Hobi
      
        
        Kendaraan
      
        
        Baju
      
        
        Elektronik
      
        
        Kesehatan
  

  
      
        
  
      

    
      sendal kulit manggis
      Kendaraan
      Rp 2.431
    
  

      
      
        
  
      

    
      Alufeed
      Kendaraan
      Rp 16.000
    
  

      
      
        
  
      

    
      produk wididay
      Kendaraan
      Rp 1.000
    
  

      
      
        
  
      

    
      Hp
      Elektronik
      Rp 50.000
    
  

      
      
        
  
      

    
      Skin keren
      Kesehatan
      Rp 32
    
  

      
      
        
  
      

    
      Orchid Malevolence
      Hobi
      Rp 198.767
    
  

      
      
        
  
      

    
      New edit
      Hobi
      Rp 10.000
    
  

      
      
        
  
      

    
      Game
      Hobi
      Rp 9
    
  

      
      
        
  
      

    
      erewr43
      Kendaraan
      Rp 55.555
    
  

      
      
        
  
      

    
      adadad
      Kendaraan
      Rp 2.312
    
  

      

    ← Previous Next →
  
</value>
      <webElementGuid>22a57322-04a0-42ce-9189-370785e2dc66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container&quot;]</value>
      <webElementGuid>35cf4d59-f81f-48da-9a74-eb4b501a0c30</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/following::section[1]</value>
      <webElementGuid>ff3013d5-0ff0-48a1-994f-e5feee7872b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan RamadhanBanyak diskon!'])[1]/following::section[1]</value>
      <webElementGuid>ef8b50d9-6cb5-47d2-b448-1ffd99303289</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[2]</value>
      <webElementGuid>a7d7d186-7071-4867-9642-7cc90ff86baf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = '
  Telusuri Kategori

  
    
      
      Semua

      
        
        Hobi
      
        
        Kendaraan
      
        
        Baju
      
        
        Elektronik
      
        
        Kesehatan
  

  
      
        
  
      

    
      sendal kulit manggis
      Kendaraan
      Rp 2.431
    
  

      
      
        
  
      

    
      Alufeed
      Kendaraan
      Rp 16.000
    
  

      
      
        
  
      

    
      produk wididay
      Kendaraan
      Rp 1.000
    
  

      
      
        
  
      

    
      Hp
      Elektronik
      Rp 50.000
    
  

      
      
        
  
      

    
      Skin keren
      Kesehatan
      Rp 32
    
  

      
      
        
  
      

    
      Orchid Malevolence
      Hobi
      Rp 198.767
    
  

      
      
        
  
      

    
      New edit
      Hobi
      Rp 10.000
    
  

      
      
        
  
      

    
      Game
      Hobi
      Rp 9
    
  

      
      
        
  
      

    
      erewr43
      Kendaraan
      Rp 55.555
    
  

      
      
        
  
      

    
      adadad
      Kendaraan
      Rp 2.312
    
  

      

    ← Previous Next →
  
' or . = '
  Telusuri Kategori

  
    
      
      Semua

      
        
        Hobi
      
        
        Kendaraan
      
        
        Baju
      
        
        Elektronik
      
        
        Kesehatan
  

  
      
        
  
      

    
      sendal kulit manggis
      Kendaraan
      Rp 2.431
    
  

      
      
        
  
      

    
      Alufeed
      Kendaraan
      Rp 16.000
    
  

      
      
        
  
      

    
      produk wididay
      Kendaraan
      Rp 1.000
    
  

      
      
        
  
      

    
      Hp
      Elektronik
      Rp 50.000
    
  

      
      
        
  
      

    
      Skin keren
      Kesehatan
      Rp 32
    
  

      
      
        
  
      

    
      Orchid Malevolence
      Hobi
      Rp 198.767
    
  

      
      
        
  
      

    
      New edit
      Hobi
      Rp 10.000
    
  

      
      
        
  
      

    
      Game
      Hobi
      Rp 9
    
  

      
      
        
  
      

    
      erewr43
      Kendaraan
      Rp 55.555
    
  

      
      
        
  
      

    
      adadad
      Kendaraan
      Rp 2.312
    
  

      

    ← Previous Next →
  
')]</value>
      <webElementGuid>f7b5a1a1-d074-4083-8b70-771f85f245d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
