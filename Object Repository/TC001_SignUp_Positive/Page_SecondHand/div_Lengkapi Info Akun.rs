<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Lengkapi Info Akun</name>
   <tag></tag>
   <elementGuidId>bdb2660b-694b-4353-99da-3c7b42945679</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.fs-6.fw-bold.text-center.position-absolute.top-50.start-50.translate-middle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dbdb9708-55d3-413f-9a8e-4908c81879b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle</value>
      <webElementGuid>7b9618c9-095b-47ac-b468-9e0501e1f5f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Lengkapi Info Akun
      </value>
      <webElementGuid>cb3ac0e7-f421-4db4-bd55-b7a4722531f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle&quot;]</value>
      <webElementGuid>facc8935-204c-4cf8-a36b-d902e87c2680</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      <webElementGuid>cdd459f8-3b8a-4df5-a2e2-a822a03b8982</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota'])[1]/preceding::div[6]</value>
      <webElementGuid>b0384544-ad55-48f0-ad4b-722d7429eded</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lengkapi Info Akun']/parent::*</value>
      <webElementGuid>96343675-3a0c-46ce-9456-4ccda34c2229</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>fd292889-69fe-4bbb-b013-7ec06b691f57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Lengkapi Info Akun
      ' or . = '
        Lengkapi Info Akun
      ')]</value>
      <webElementGuid>0f153867-cb07-4a14-8bc9-5016d5422bb1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
