<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email_useremail</name>
   <tag></tag>
   <elementGuidId>c38dca60-761e-4ba7-938b-1f5c3053de7a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='user_email']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#user_email</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c04498b7-ca22-4e16-828f-9b75088d8f66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>5c539d4b-ad8c-4888-bcda-911bc7b57f4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autofocus</name>
      <type>Main</type>
      <value>autofocus</value>
      <webElementGuid>274703a6-df95-4c01-bdfb-522e50de3112</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>28a754a6-fe21-42ea-8151-b35a3403dbf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>b22fffa7-796d-48bf-ae3d-b22f78a176c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: johndee@gmail.com</value>
      <webElementGuid>04064bfa-2862-42ca-a709-6947e2284e6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>e48c390c-f40a-4b9a-9956-02746866be62</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user[email]</value>
      <webElementGuid>157e965d-5c66-456d-b47d-54e936a65871</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_email</value>
      <webElementGuid>356cd339-4e55-4962-85c6-49f992e00815</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user_email&quot;)</value>
      <webElementGuid>3459d9db-0f99-4979-b30f-6e9c7af84085</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='user_email']</value>
      <webElementGuid>3fc7c4d4-721c-4cb4-9acf-c431cbff4c1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[2]/input</value>
      <webElementGuid>4b0822a1-d777-47fc-8bf3-70ef7040487a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>54c8ed47-4f87-4a6e-bce8-c44d0b8ceb71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Contoh: johndee@gmail.com' and @type = 'email' and @name = 'user[email]' and @id = 'user_email']</value>
      <webElementGuid>d3554487-5f84-4066-9611-c4f70072d597</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
