<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Masuk                              Emai_4d76ca</name>
   <tag></tag>
   <elementGuidId>f4ac7290-ac90-4c56-911f-c3c52231db30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.container.p-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>08488b7b-3767-486e-8c82-02321b970184</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container p-5</value>
      <webElementGuid>8dc37cc9-d64f-4153-85fc-b8c22d8cd510</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      </value>
      <webElementGuid>5fa66dfa-3df5-4297-bf48-2c06d2fc8981</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;container-fluid min-height-100vh&quot;]/div[@class=&quot;row min-height-100vh w-100&quot;]/div[@class=&quot;col-6 d-flex flex-column align-items-center justify-content-center&quot;]/div[@class=&quot;container p-5&quot;]</value>
      <webElementGuid>09ed7203-128b-44fe-a9c9-574781e2b340</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::div[2]</value>
      <webElementGuid>673cc757-c8e1-40f5-88b1-022a505d05f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div</value>
      <webElementGuid>1461cecb-6053-4b02-a865-fa2d395a2941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      ' or . = '
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      ')]</value>
      <webElementGuid>b0671fc1-271e-4ea8-b325-18df995a09df</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
