<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_HP Samsung      Elektronik      Rp 2.000.000</name>
   <tag></tag>
   <elementGuidId>2ca03537-4999-4a03-b1c9-5d3457d77caa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'produk wididay' or . = 'produk wididay')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/section/div/div[2]/div[2]/div/div[7]/a/div</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>85232458-318f-4e45-8202-b4964dbcca1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>66cae447-49bc-470b-907b-bb470cb9267c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>produk wididay</value>
      <webElementGuid>4d64ec1e-1d4c-45b4-84d4-38736b1bb77e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>1f7a8779-1a31-49f2-90f8-1f9a3ab9bddc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.000.000'])[1]/following::div[2]</value>
      <webElementGuid>1095ccc9-6922-4196-b4fc-41e9622de3ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP Samsung'])[1]/following::div[2]</value>
      <webElementGuid>fa713400-078b-4723-8753-f7e2a88c038b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/a/div</value>
      <webElementGuid>890e497b-b6ae-4cc2-b440-bceb7c2df0c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      HP Samsung
      Elektronik
      Rp 2.000.000
    
  ' or . = '
      

    
      HP Samsung
      Elektronik
      Rp 2.000.000
    
  ')]</value>
      <webElementGuid>a3f6805a-2647-4d7d-8a31-6ed0b87b03d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
