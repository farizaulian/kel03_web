<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Akio HP Samsung      Elektronik      Rp 2.000.000</name>
   <tag></tag>
   <elementGuidId>38b19eff-b351-4132-bf6a-e07870b2f6a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'produk wididay' or . = 'produk wididay')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c21085d6-78c4-42d2-ab76-51c7ca621399</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-12 col-md-4 col-lg-3</value>
      <webElementGuid>a6546823-3399-4503-bb03-a993b35bef49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>produk wididay</value>
      <webElementGuid>878b367d-137f-4673-82df-39b7832be12c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/section/div/div[2]/div[2]/div/div[2]/a/div/div/h5[1]</value>
      <webElementGuid>831f4f3b-8643-4359-a45d-2a8fdd9b2486</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div[6]</value>
      <webElementGuid>ee812fc8-5db9-4b7f-a70d-90cf29883de1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.000.000'])[4]/following::div[1]</value>
      <webElementGuid>c6b9159b-0da9-4c11-8582-9523b9721799</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HP Samsung'])[4]/following::div[1]</value>
      <webElementGuid>437e5475-8275-4ef6-bee2-90d075807fb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]</value>
      <webElementGuid>2ec87940-3917-4e2f-9ac1-b269ce6fef24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
  
      

    
      Akio HP Samsung
      Elektronik
      Rp 2.000.000
    
  

      ' or . = '
        
  
      

    
      Akio HP Samsung
      Elektronik
      Rp 2.000.000
    
  

      ')]</value>
      <webElementGuid>946c572f-6cd6-4826-afe1-fabd8aac0e92</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
