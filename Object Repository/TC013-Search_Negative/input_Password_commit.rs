<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_commit</name>
   <tag></tag>
   <elementGuidId>18450948-d28f-4208-b0f8-99e9d03d2ebb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;commit&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='commit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d9f14fc4-8b87-4cdf-a0ed-a5bfc4991d6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>b52b7a81-35a5-4ce9-800a-15f9d9e04a54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>commit</value>
      <webElementGuid>e10a55c5-afce-4e93-aa78-ca1b5b31a3b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>b57b107a-c193-4b38-b705-6c65b21a2ff3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary w-100 rounded-4 p-3 fw-bold</value>
      <webElementGuid>14a470a4-b12b-4c8b-8e88-0d90c61d7c0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-disable-with</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>7936ac17-c7eb-4318-a898-d77047e74095</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;actions&quot;]/input[@class=&quot;btn btn-primary w-100 rounded-4 p-3 fw-bold&quot;]</value>
      <webElementGuid>4c99fd11-c8e6-44c2-93c6-5d9ceb08689a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='commit']</value>
      <webElementGuid>f7bd38f7-8311-4343-8246-037dcb2fbb8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[3]/input</value>
      <webElementGuid>0a51af4f-b519-4eb9-ab3f-961373eed2e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>11746017-8dae-4771-a344-675036ca44fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'commit']</value>
      <webElementGuid>1c40f5dd-054d-4a6b-bf3a-9c079fbe63ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
