<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Lengkapi Info Akun</name>
   <tag></tag>
   <elementGuidId>080e21d6-1628-4dd1-9b90-ae030330c956</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.fs-6.fw-bold.text-center.position-absolute.top-50.start-50.translate-middle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b35c5d5b-594d-4f0f-b4da-c2658e4dadee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle</value>
      <webElementGuid>b8cf6370-936e-416b-b2d9-aa1faa3a2145</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Lengkapi Info Akun
      </value>
      <webElementGuid>a0ff415c-1b88-472a-a38c-8db6408303ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle&quot;]</value>
      <webElementGuid>af067dd6-258e-4f28-9e71-48d16db0708a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      <webElementGuid>b22958f1-a52b-400b-9ef2-1a1c080060b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota'])[1]/preceding::div[6]</value>
      <webElementGuid>3cf19616-fe8b-473c-924b-6e1ba2ac8778</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lengkapi Info Akun']/parent::*</value>
      <webElementGuid>09869ad3-37c9-4ab2-96f3-a4739b322c95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>8143e136-051d-42d3-bc20-0ca6457fd634</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Lengkapi Info Akun
      ' or . = '
        Lengkapi Info Akun
      ')]</value>
      <webElementGuid>c527a510-f725-4218-84ab-258ee504c9a5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
