<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Daftar Jual Saya                   _0b6c3c</name>
   <tag></tag>
   <elementGuidId>6f8efcce-6dd3-4ff7-a560-ccf27dbfcda0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>section.pt-5.mt-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::section[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>96ddb237-1f54-4ac6-9351-5211255287af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pt-5 mt-5</value>
      <webElementGuid>3194a24b-a469-49a4-b2b0-74b0d0a67871</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
  Daftar Jual Saya

  
    
        

      
        Shantika Ardita
        Jakarta
      

      Edit
    
  

  
    
      
        Kategori

        
          
            
              
              Semua Produk
              

            
              
              Diminati
              

            
              
              Terjual
              
          
        
      
    
    
      
          
            
              
                
                Tambah Produk
              
          

            
              
  
      

    
      Laptop
      Elektronik
      Rp 20.000.000
    
  

            
            
              
  
      

    
      Sisir
      Hobi
      Rp 15.000
    
  

            
            
              
  
      

    
      HP Samsung
      Elektronik
      Rp 2.000.000
    
  

            
            
              
  
      

    
      Gelas
      Hobi
      Rp 10.000
    
  

            
            
              
  
      

    
      Gelas
      Hobi
      Rp 10.000
    
  

            

          
      
    
  


    </value>
      <webElementGuid>060985a7-d28f-4401-8944-c19a5b1512bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]</value>
      <webElementGuid>fb70ed72-de06-4d80-9f8c-2ee9b836fb46</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::section[1]</value>
      <webElementGuid>24949301-e14b-43d4-bbb9-32203308e349</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>033aad83-f4bb-488e-a1dd-a735ae56bc05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = '
      
  Daftar Jual Saya

  
    
        

      
        Shantika Ardita
        Jakarta
      

      Edit
    
  

  
    
      
        Kategori

        
          
            
              
              Semua Produk
              

            
              
              Diminati
              

            
              
              Terjual
              
          
        
      
    
    
      
          
            
              
                
                Tambah Produk
              
          

            
              
  
      

    
      Laptop
      Elektronik
      Rp 20.000.000
    
  

            
            
              
  
      

    
      Sisir
      Hobi
      Rp 15.000
    
  

            
            
              
  
      

    
      HP Samsung
      Elektronik
      Rp 2.000.000
    
  

            
            
              
  
      

    
      Gelas
      Hobi
      Rp 10.000
    
  

            
            
              
  
      

    
      Gelas
      Hobi
      Rp 10.000
    
  

            

          
      
    
  


    ' or . = '
      
  Daftar Jual Saya

  
    
        

      
        Shantika Ardita
        Jakarta
      

      Edit
    
  

  
    
      
        Kategori

        
          
            
              
              Semua Produk
              

            
              
              Diminati
              

            
              
              Terjual
              
          
        
      
    
    
      
          
            
              
                
                Tambah Produk
              
          

            
              
  
      

    
      Laptop
      Elektronik
      Rp 20.000.000
    
  

            
            
              
  
      

    
      Sisir
      Hobi
      Rp 15.000
    
  

            
            
              
  
      

    
      HP Samsung
      Elektronik
      Rp 2.000.000
    
  

            
            
              
  
      

    
      Gelas
      Hobi
      Rp 10.000
    
  

            
            
              
  
      

    
      Gelas
      Hobi
      Rp 10.000
    
  

            

          
      
    
  


    ')]</value>
      <webElementGuid>2aff2de7-078c-499b-9d6e-7118413a679d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
