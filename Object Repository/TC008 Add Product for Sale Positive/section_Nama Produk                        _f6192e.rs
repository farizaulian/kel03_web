<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Nama Produk                        _f6192e</name>
   <tag></tag>
   <elementGuidId>14dc0986-387a-44d8-ac21-1c71ac628bd3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>section.pt-5.mt-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::section[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>0d40d3ab-380f-4d42-abe6-388716e69cbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pt-5 mt-5</value>
      <webElementGuid>84287068-f561-4507-8660-d419386e9b5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
  
  
    
  


  
  
    
      
        Nama Produk
        
      

      
        Harga Produk
        
      

      
        Kategori
        Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan
      

      
        Deskripsi
        
      

      

        
  




      

      
        
        Preview

        
        Terbitkan
      
  




  function clickFileInput(element) {
    const input = element.nextElementSibling.nextElementSibling
    input.previousFiles = input.files;
    input.addEventListener('change', onImageInputElementChanged)

    input.click();
  }

  function onImageInputElementChanged(e) {
    if (e.target.files.length === 0) return;

    const dt = new DataTransfer();

    for (const file of e.target.previousFiles) {
      dt.items.add(file);
    }

    for (const file of e.target.files) {
      let div = document.createElement('div');
      div.classList.add('form-image-preview', 'position-relative', 'rounded-4', 'overflow-hidden');

      let img = document.createElement('img');
      img.classList.add(&quot;img-preview&quot;);
      img.src = URL.createObjectURL(file);

      let button = document.createElement('button');
      button.type = 'button';
      button.ariaLabel = 'close';
      button.style = &quot;transform: translate(-150%, 50%) !important&quot;;
      button.dataset.inputId = e.target.id;
      button.file = file;
      button.classList.add('btn-close', 'btn-close-white', 'position-absolute', 'top-0', 'start-100', 'translate-middle');
      button.addEventListener('click', onImageInputRemoveButtonClicked)

      div.append(img);
      div.append(button);

      dt.items.add(file);
      e.target.previousElementSibling.previousElementSibling.before(div);
    }

    e.target.previousFiles = null;
    e.target.files = dt.files;
  }

  function onImageInputRemoveButtonClicked(e) {
    const input = document.getElementById(e.target.dataset.inputId);
    const dt = new DataTransfer()

    for (let file of input.files) {
      if (file.name !== e.target.file.name) {
        dt.items.add(file)
      }
    }

    input.files = dt.files;

    e.target.parentElement.remove();
  }




    </value>
      <webElementGuid>0d3ae83a-d7ca-4810-bbeb-afcaa5792d68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]</value>
      <webElementGuid>a91f61f3-5b18-4271-9578-1fd2c5d80de1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::section[1]</value>
      <webElementGuid>6f75768c-550c-4499-bb7a-aa225be34ab3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>ef4fb08b-84f1-458a-a9e9-16c324c3c873</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = concat(&quot;
      
  
  
    
  


  
  
    
      
        Nama Produk
        
      

      
        Harga Produk
        
      

      
        Kategori
        Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan
      

      
        Deskripsi
        
      

      

        
  




      

      
        
        Preview

        
        Terbitkan
      
  




  function clickFileInput(element) {
    const input = element.nextElementSibling.nextElementSibling
    input.previousFiles = input.files;
    input.addEventListener(&quot; , &quot;'&quot; , &quot;change&quot; , &quot;'&quot; , &quot;, onImageInputElementChanged)

    input.click();
  }

  function onImageInputElementChanged(e) {
    if (e.target.files.length === 0) return;

    const dt = new DataTransfer();

    for (const file of e.target.previousFiles) {
      dt.items.add(file);
    }

    for (const file of e.target.files) {
      let div = document.createElement(&quot; , &quot;'&quot; , &quot;div&quot; , &quot;'&quot; , &quot;);
      div.classList.add(&quot; , &quot;'&quot; , &quot;form-image-preview&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;position-relative&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;rounded-4&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;overflow-hidden&quot; , &quot;'&quot; , &quot;);

      let img = document.createElement(&quot; , &quot;'&quot; , &quot;img&quot; , &quot;'&quot; , &quot;);
      img.classList.add(&quot;img-preview&quot;);
      img.src = URL.createObjectURL(file);

      let button = document.createElement(&quot; , &quot;'&quot; , &quot;button&quot; , &quot;'&quot; , &quot;);
      button.type = &quot; , &quot;'&quot; , &quot;button&quot; , &quot;'&quot; , &quot;;
      button.ariaLabel = &quot; , &quot;'&quot; , &quot;close&quot; , &quot;'&quot; , &quot;;
      button.style = &quot;transform: translate(-150%, 50%) !important&quot;;
      button.dataset.inputId = e.target.id;
      button.file = file;
      button.classList.add(&quot; , &quot;'&quot; , &quot;btn-close&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;btn-close-white&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;position-absolute&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;top-0&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;start-100&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;translate-middle&quot; , &quot;'&quot; , &quot;);
      button.addEventListener(&quot; , &quot;'&quot; , &quot;click&quot; , &quot;'&quot; , &quot;, onImageInputRemoveButtonClicked)

      div.append(img);
      div.append(button);

      dt.items.add(file);
      e.target.previousElementSibling.previousElementSibling.before(div);
    }

    e.target.previousFiles = null;
    e.target.files = dt.files;
  }

  function onImageInputRemoveButtonClicked(e) {
    const input = document.getElementById(e.target.dataset.inputId);
    const dt = new DataTransfer()

    for (let file of input.files) {
      if (file.name !== e.target.file.name) {
        dt.items.add(file)
      }
    }

    input.files = dt.files;

    e.target.parentElement.remove();
  }




    &quot;) or . = concat(&quot;
      
  
  
    
  


  
  
    
      
        Nama Produk
        
      

      
        Harga Produk
        
      

      
        Kategori
        Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan
      

      
        Deskripsi
        
      

      

        
  




      

      
        
        Preview

        
        Terbitkan
      
  




  function clickFileInput(element) {
    const input = element.nextElementSibling.nextElementSibling
    input.previousFiles = input.files;
    input.addEventListener(&quot; , &quot;'&quot; , &quot;change&quot; , &quot;'&quot; , &quot;, onImageInputElementChanged)

    input.click();
  }

  function onImageInputElementChanged(e) {
    if (e.target.files.length === 0) return;

    const dt = new DataTransfer();

    for (const file of e.target.previousFiles) {
      dt.items.add(file);
    }

    for (const file of e.target.files) {
      let div = document.createElement(&quot; , &quot;'&quot; , &quot;div&quot; , &quot;'&quot; , &quot;);
      div.classList.add(&quot; , &quot;'&quot; , &quot;form-image-preview&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;position-relative&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;rounded-4&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;overflow-hidden&quot; , &quot;'&quot; , &quot;);

      let img = document.createElement(&quot; , &quot;'&quot; , &quot;img&quot; , &quot;'&quot; , &quot;);
      img.classList.add(&quot;img-preview&quot;);
      img.src = URL.createObjectURL(file);

      let button = document.createElement(&quot; , &quot;'&quot; , &quot;button&quot; , &quot;'&quot; , &quot;);
      button.type = &quot; , &quot;'&quot; , &quot;button&quot; , &quot;'&quot; , &quot;;
      button.ariaLabel = &quot; , &quot;'&quot; , &quot;close&quot; , &quot;'&quot; , &quot;;
      button.style = &quot;transform: translate(-150%, 50%) !important&quot;;
      button.dataset.inputId = e.target.id;
      button.file = file;
      button.classList.add(&quot; , &quot;'&quot; , &quot;btn-close&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;btn-close-white&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;position-absolute&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;top-0&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;start-100&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;translate-middle&quot; , &quot;'&quot; , &quot;);
      button.addEventListener(&quot; , &quot;'&quot; , &quot;click&quot; , &quot;'&quot; , &quot;, onImageInputRemoveButtonClicked)

      div.append(img);
      div.append(button);

      dt.items.add(file);
      e.target.previousElementSibling.previousElementSibling.before(div);
    }

    e.target.previousFiles = null;
    e.target.files = dt.files;
  }

  function onImageInputRemoveButtonClicked(e) {
    const input = document.getElementById(e.target.dataset.inputId);
    const dt = new DataTransfer()

    for (let file of input.files) {
      if (file.name !== e.target.file.name) {
        dt.items.add(file)
      }
    }

    input.files = dt.files;

    e.target.parentElement.remove();
  }




    &quot;))]</value>
      <webElementGuid>3969188b-c982-4191-894f-fc8dbaeba3cc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
