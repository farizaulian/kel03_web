<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_kel03_03              Jogja</name>
   <tag></tag>
   <elementGuidId>86514b0c-1912-4ded-959b-2cf67c7d4d21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.d-flex.align-items-center.justify-content.center.flex-column.py-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/ul/li/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c8763bdd-1aaa-42d1-b1be-0824e69b55fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex align-items-center justify-content center flex-column py-2</value>
      <webElementGuid>ec650818-845e-4fbd-9e56-404f1fea67c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              

            
              kel03_03
              Jogja
            
          </value>
      <webElementGuid>27e87386-e223-45c6-8f1b-9fcc5a9c76fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown fs-5 d-none d-lg-block&quot;]/ul[@class=&quot;dropdown-menu show&quot;]/li[1]/a[@class=&quot;nav-user&quot;]/div[@class=&quot;d-flex align-items-center justify-content center flex-column py-2&quot;]</value>
      <webElementGuid>e470fa09-a569-4cc4-8049-8b4f22330904</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/ul/li/a/div</value>
      <webElementGuid>b5fa8376-8a60-41c6-b3fb-6ebc71d37343</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[2]/following::div[1]</value>
      <webElementGuid>25e79601-f48e-4fa0-b67a-d916b7723ae4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/following::div[1]</value>
      <webElementGuid>509e0bae-0d59-41e1-b7ab-9a7998f3b3b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/ul/li/a/div</value>
      <webElementGuid>2edac383-0a46-480e-b923-04c6224ed80e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
              

            
              kel03_03
              Jogja
            
          ' or . = '
              

            
              kel03_03
              Jogja
            
          ')]</value>
      <webElementGuid>71242dbe-bf01-46c5-bb7f-84336a4f6545</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
