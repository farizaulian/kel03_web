<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menunggu Respon Penjuall</name>
   <tag></tag>
   <elementGuidId>cbc44d94-ab0c-4b97-8844-4fe2619c8499</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.p-2.rounded-4.shadow.border-0 > div.card-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5fd909df-f61b-4ada-977f-051a07914e31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
      <webElementGuid>dfa485a6-a74e-4ae4-8244-8e3da9f3c7eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          Jual kesehatan
          Kesehatan
          Rp 1.000.000
            Menunggu respon penjual
        </value>
      <webElementGuid>33c222f5-dc9a-4519-bbff-f00e501b5e66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card p-2 rounded-4 shadow border-0&quot;]/div[@class=&quot;card-body&quot;]</value>
      <webElementGuid>e6369d12-794d-4692-8e55-ebeb3222ad30</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::div[3]</value>
      <webElementGuid>838f0902-6c3b-4b07-a4df-6afb601cd946</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div</value>
      <webElementGuid>60e07dbd-c25a-4486-bdff-a0b4be003de5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
          Jual kesehatan
          Kesehatan
          Rp 1.000.000
            Menunggu respon penjual
        ' or . = '
          Jual kesehatan
          Kesehatan
          Rp 1.000.000
            Menunggu respon penjual
        ')]</value>
      <webElementGuid>58f1817f-3209-4ecd-bef9-74301bab019d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
