import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('TC008 Add Product for Sale Positive/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Email_useremail'), 'Shantika113@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Password_userpassword'), 
    'aeHFOx8jV/A=')

WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Password_commit'))

WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/i_Produk Saya_bi bi-list-ul me-4 me-lg-0'))

WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/i_Terjual_bi bi-plus display-3'))

WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Nama Produk_productname'), 'HP Samsung')

WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Harga Produk_productprice'), '2000000')

WebUI.selectOptionByValue(findTestObject('Object Repository/TC008 Add Product for Sale Positive/select_Pilih KategoriHobiKendaraanBajuElekt_20972a'), 
    '4', true)

WebUI.setText(findTestObject('Object Repository/TC008 Add Product for Sale Positive/textarea_Deskripsi_productdescription'), 
    'Samsung A50S')

WebUI.click(findTestObject('TC008 Add Product for Sale Positive/add_gambar_stock'))

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/label_Preview'))

WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/input_Password_commit'))

WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/i_Produk Saya_bi bi-list-ul me-4 me-lg-0'))

WebUI.click(findTestObject('Object Repository/TC008 Add Product for Sale Positive/section_Daftar Jual Saya                   _0b6c3c'))

WebUI.closeBrowser()

