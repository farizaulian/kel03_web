import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/a_Masuk'))

WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/div_Masuk                              Emai_4d76ca'))

WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/a_Daftar di sini'))

WebUI.setText(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Name_username'), 'QAE10_GR03')

Date email = new Date()

String emailDoctor = email.format('yyyyMMddHHmmss')

def email_code = ('QAE10_GR03_' + emailDoctor) + '@gmail.com'

WebUI.setText(findTestObject('Page_SecondHand/input_Email_useremail1'), email_code)

//WebUI.setText(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Email_useremail'), 'kel03_03@gmail.com')
WebUI.setEncryptedText(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Password_userpassword'), 
    'AtJXY6uUvnA=')

WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/input_Harga Tawar_commit'))

WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/i_Keluar_bi bi-person me-4 me-lg-0'))

WebUI.click(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/img_Profil Saya_img-avatar w-100 rounded-4 mb-2'))

WebUI.verifyElementVisible(findTestObject('Object Repository/TC001_SignUp_Positive/Page_SecondHand/div_Lengkapi Info Akun'))

