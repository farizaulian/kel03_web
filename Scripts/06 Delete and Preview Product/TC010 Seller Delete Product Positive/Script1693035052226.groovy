import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://secondhand.binaracademy.org/')

WebUI.click(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/a_Masuk'))

WebUI.setText(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/input_Email_useremail'), 'kel03_03@gmail.com')

WebUI.setText(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/input_Password_userpassword'), 'kel03')

WebUI.click(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/input_Password_commit'))

WebUI.click(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/a_Produk Saya'))

WebUI.scrollToElement(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/Page_SecondHand/h5_produk wididay'), 
    5)

WebUI.click(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/Page_SecondHand/h5_produk wididay'))

WebUI.click(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/a_Delete'))

WebUI.verifyElementVisible(findTestObject('TC010_SellerDeleteProduct_Positif/Page_SecondHand/Daftar Produk Saya'))

