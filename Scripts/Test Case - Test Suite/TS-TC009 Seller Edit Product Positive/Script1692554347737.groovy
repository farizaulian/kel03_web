import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/TC009 Seller Edit Product Positive/i_Produk Saya_bi bi-list-ul me-4 me-lg-0'))

WebUI.click(findTestObject('Object Repository/TC009 Seller Edit Product Positive/h5_HP Samsung'))

WebUI.click(findTestObject('Object Repository/TC009 Seller Edit Product Positive/a_Edit'))

WebUI.setText(findTestObject('Object Repository/TC009 Seller Edit Product Positive/input_Nama Produk_productname'), 'produk wididay')

WebUI.setText(findTestObject('Object Repository/TC009 Seller Edit Product Positive/input_Harga Produk_productprice'), '450000')

WebUI.click(findTestObject('Object Repository/TC009 Seller Edit Product Positive/section_Nama Produk                        _981ce4'))

WebUI.click(findTestObject('Object Repository/TC009 Seller Edit Product Positive/label_Terbitkan'))

WebUI.click(findTestObject('Object Repository/TC009 Seller Edit Product Positive/i_Produk Saya_bi bi-list-ul me-4 me-lg-0'))

WebUI.click(findTestObject('Object Repository/TC009 Seller Edit Product Positive/div_HP Samsung 1      Elektronik      Rp 3.000.000'))

WebUI.delay(2)

WebUI.click(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/dashboard'))

