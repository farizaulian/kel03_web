import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/i_Next_bi bi-plus fs-2 me-3'))

WebUI.setText(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/input_Nama Produk_productname'), 'produk wididay')

WebUI.setText(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/input_Harga Produk_productprice'), 
    '1000')

WebUI.selectOptionByValue(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/select_Pilih KategoriHobiKendaraanBajuElektronikKesehatan'), 
    kategori, false)

WebUI.setText(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/textarea_Deskripsi_productdescription'), 
    'jalan')

WebUI.uploadFile(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/i_Deskripsi_bi bi-plus display-6'), '/Users/telkom/Desktop/dummy.png')

WebUI.delay(5)

//WebUI.verifyImagePresent(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/img_Deskripsi_img-preview'))
WebUI.click(findTestObject('Object Repository/TC006_AddNewProduct_Positive/Page_SecondHand/label_Terbitkan'))

WebUI.verifyElementVisible(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/page preview upload product'))

WebUI.click(findTestObject('TC006_AddNewProduct_Positive/Page_SecondHand/dashboard'))

